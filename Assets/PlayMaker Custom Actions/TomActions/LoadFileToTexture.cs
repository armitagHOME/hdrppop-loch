// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using System.IO;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.Application)]
	[Tooltip("Loads a file from the Assets/ folder and stores it as a Texture")]
	public class LoadFileToTexture : FsmStateAction
	{
		[Tooltip("The path of the file relative to the Assets/ folder")]
		public FsmString filePath;

		[Tooltip("The name of the file INCLUDING the file extension")]
		public FsmString fileName;

		[Tooltip("Store file to texture.")]
		[UIHint(UIHint.Variable), RequiredField, Readonly]
		public FsmTexture storeTexture;

		public override void Reset()
		{
			filePath = null;
		}

		public override void OnEnter()
		{
			var bytes = File.ReadAllBytes(Application.dataPath + "/" + filePath.Value + fileName.Value);
			var tex = new Texture2D(2,2);
			tex.LoadImage(bytes);
			storeTexture.Value = tex;

			Finish();
		}

		
	}
}

