﻿using UnityEngine;
using HutongGames.PlayMaker;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.Renderer)]
	[Tooltip("Sets the positions of a lineRenderer based on a Vector3 array")]
	public class SetLineRendererPositions : FsmStateAction
	{
		public enum UpdateType { OnUpdate, OnFixedUpdate, OnLateUpdate }

		[RequiredField]
		[Tooltip("The GameObject with the LineRenderer component.")]
		[CheckForComponent(typeof(LineRenderer))]
		public FsmOwnerDefault gameObject;

		[Tooltip("The Position array")]
		[UIHint(UIHint.Variable), RequiredField, Readonly]
		[ArrayEditor(VariableType.Vector3)]
		public FsmArray positionArray;

		[Tooltip("Repeat every frame.")]
		public bool everyFrame;

		public UpdateType updateType;

		Vector3[] _positions;
		GameObject _go;
		LineRenderer _lr;

		public override void Reset()
		{
			gameObject = null;
			positionArray = null;
			updateType = UpdateType.OnUpdate;
		}

		public override void OnPreprocess()
		{
			if (updateType == UpdateType.OnFixedUpdate)	Fsm.HandleFixedUpdate = true;

#if PLAYMAKER_1_8_5_OR_NEWER
			if (updateType == UpdateType.OnLateUpdate) Fsm.HandleLateUpdate = true;
#endif
		}

		public override void OnUpdate()
		{
			if (updateType == UpdateType.OnUpdate)
			{
				Execute();
			}

			if (!everyFrame)
			{
				Finish();
			}
		}

		public override void OnLateUpdate()
		{
			if (updateType == UpdateType.OnLateUpdate)
			{
				Execute();
			}

			if (!everyFrame)
			{
				Finish();
			}
		}

		public override void OnFixedUpdate()
		{
			if (updateType == UpdateType.OnFixedUpdate)
			{
				Execute();
			}

			if (!everyFrame)
			{
				Finish();
			}
		}

		void Execute()
		{
			_go = Fsm.GetOwnerDefaultTarget(gameObject);
			if (!_go) return;

			_lr = _go.GetComponent<LineRenderer>();
			if (!_lr) return;

			_positions = new Vector3[positionArray.Values.Length];
			for (int i = 0; i < positionArray.Length; i++)
				_positions[i] = (Vector3)positionArray.Values[i];

			_lr.positionCount = _positions.Length;
			_lr.SetPositions(_positions);

		}
	}
}
