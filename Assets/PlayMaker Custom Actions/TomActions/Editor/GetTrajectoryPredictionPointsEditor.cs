﻿using HutongGames.PlayMaker.Actions;
using HutongGames.PlayMakerEditor;
using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEditorInternal;

namespace HutongGames.PlayMakerEditor
{
    [CustomActionEditor(typeof(GetTrajectoryPredictionPoints))]
    public class GetTrajectoryPredictionPointsEditor : CustomActionEditor
    {

        private GetTrajectoryPredictionPoints action;
        private GUIStyle bold;
        public override void OnEnable()
        {
            action = (GetTrajectoryPredictionPoints)target;
            bold = new GUIStyle { fontStyle = FontStyle.Bold };
        }

        public override bool OnGUI()
        {
            EditField("startPosition");
            if (action.startPosition.GameObject.IsNone)
                EditField("startPositionVector");
            EditField("rigidbodyObject");
            EditField("velocityDirection");
            EditField("velocityForce");
            EditField("frameCount");
            EditField("timeStepMultiplier");
            EditField("storePositions");
            GUILayout.Space(3);
            EditorGUILayout.LabelField("Collision Options:", bold);
            EditField("detectCollision");
            if (action.detectCollision)
            {
                int temp = EditorGUILayout.MaskField("CollisionMask", InternalEditorUtility.LayerMaskToConcatenatedLayersMask(action.collisionMask), InternalEditorUtility.layers);
                action.collisionMask = InternalEditorUtility.ConcatenatedLayersMaskToLayerMask(temp);
                EditField("clampPositionArrayToCollision");
                EditField("collisionObject");
                EditField("collisionPoint");
                EditField("collisionNormal");
            }
            EditField("everyFrame");

            return GUI.changed;
        }
    }
}
