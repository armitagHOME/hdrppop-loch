// License: Attribution 4.0 International (CC BY 4.0)
/*--- __ECO__ __PLAYMAKER__ __ACTION__ ---*/

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.StateMachine)]
	[Tooltip("Sends an Event after x frame. Useful if you want to loop states every x frame.")]
	public class NextFrameEventAdvance : FsmStateAction
	{
		public enum FrameType { Update, FixedUpdate, LateUpdate }

		[RequiredField]
		public FsmInt frameCount;
		public FrameType frameType;
		public FsmEvent sendEvent;

		private int loop;

		public override void Reset()
		{
			frameCount = 1;
			sendEvent = null;
			frameType = FrameType.Update;
		}

		public override void OnEnter()
		{
			loop = 0;
		}

		public override void OnPreprocess()
		{
			if (frameType == FrameType.FixedUpdate)
				Fsm.HandleFixedUpdate = true;
			else if (frameType == FrameType.LateUpdate)
				Fsm.HandleLateUpdate = true;
		}

		public override void OnUpdate()
		{
			if (frameType == FrameType.Update)
				DoNextFrame();
		}

		public override void OnFixedUpdate()
		{
			if (frameType == FrameType.FixedUpdate)
				DoNextFrame();
		}

		public override void OnLateUpdate()
		{
			if (frameType == FrameType.LateUpdate)
				DoNextFrame();
		}

		void DoNextFrame()
        {
			loop++;

			if (loop == frameCount.Value)
			{

				Finish();

				Fsm.Event(sendEvent);
			}
		}

#if UNITY_EDITOR
        public override string AutoName()
        {
            return "Next Frame Event: " + sendEvent.Name;
        }
#endif
	}
}
