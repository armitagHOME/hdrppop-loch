// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.Camera)]
	[Tooltip("Takes the current view of the game camera and converts it to an image type")]
	public class CaptureScreenshotIntoRenderTexture : FsmStateAction
	{
		[Tooltip("The render texture in your project")]
		[UIHint(UIHint.FsmObject), RequiredField]
		public FsmObject renderTexture;

		[Tooltip("Adjust the height and width on the texture to the game view size")]
		public bool autoSize;

		public override void Reset()
		{
			renderTexture = null;
		}

		public override void OnEnter()
		{
			var rendTex = new RenderTexture(Screen.width, Screen.height, 0);
			ScreenCapture.CaptureScreenshotIntoRenderTexture(rendTex);

			renderTexture.Value = rendTex;

			Finish();
		}
	}
}

