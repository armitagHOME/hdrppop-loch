// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using System.Collections.Generic;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory(ActionCategory.Physics)]
    [Tooltip("Sends events based on value of input Axis")]
    public class GetTrajectoryPredictionPoints : FsmStateAction
    {

        public enum UpdateType { OnUpdate, OnFixedUpdate, OnLateUpdate }

        [Tooltip("The start position of the object")]
        public FsmOwnerDefault startPosition;

        [Tooltip("The start position of the object")]
        public FsmVector3 startPositionVector;

        [Tooltip("The object we are predicting")]
        [RequiredField]
        [CheckForComponent(typeof(Rigidbody))]
        public FsmGameObject rigidbodyObject;

        [Tooltip("The velocity direction of the object")]
        public FsmVector3 velocityDirection;

        [Tooltip("The velocity force of the object")]
        public FsmFloat velocityForce;

        [Tooltip("The amount position points in the array")]
        public FsmInt frameCount;

        [Tooltip("The amount of physics time between each frame count. " +
            "Increase this while lowering Frame Count for better performance.")]
        public FsmFloat timeStepMultiplier;

        [Tooltip("Store the position points")]
        [UIHint(UIHint.Variable), RequiredField, Readonly]
        [ArrayEditor(VariableType.Vector3)]
        public FsmArray storePositions = null;

        [Tooltip("Detect the predicted collision")]
        public bool detectCollision;

        [Tooltip("Only include these layers")]
        public int collisionMask;

        [Tooltip("Stop gettings points if there is a collision")]
        public bool clampPositionArrayToCollision;

        [Tooltip("Store the collided object")]
        [UIHint(UIHint.Variable), Readonly]
        public FsmGameObject collisionObject;

        [Tooltip("Store the collision point")]
        [UIHint(UIHint.Variable), Readonly]
        public FsmVector3 collisionPoint;

        [Tooltip("Store the collision normal")]
        [UIHint(UIHint.Variable), Readonly]
        public FsmVector3 collisionNormal;

        [Tooltip("Repeat every frame.")]
        public bool everyFrame;

        [Tooltip("Update order options for storing data")]
        public UpdateType updateType;

        private Ray ray;
        private RaycastHit rayHit;
        private GameObject go;
        private Rigidbody rb;
        private Vector3 startPos;
        private Vector3[] posArray;
        private List<Vector3> posList;

        public override void Reset()
        {
            rigidbodyObject = null;
            velocityDirection = null;
            frameCount = null;
            timeStepMultiplier.Value = 1.0f;
            storePositions = null;
        }

        public override void OnPreprocess()
        {
            if (updateType == UpdateType.OnFixedUpdate) Fsm.HandleFixedUpdate = true;
            if (updateType == UpdateType.OnLateUpdate) Fsm.HandleLateUpdate = true;
        }

        public override void OnEnter()
        {
            if (everyFrame) return;
            DoPrediction();
            Finish();
        }

        public override void OnUpdate()
        {
            if (everyFrame && updateType == UpdateType.OnUpdate)
                DoPrediction();
        }

        public override void OnFixedUpdate()
        {
            if (everyFrame && updateType == UpdateType.OnFixedUpdate)
                DoPrediction();
        }

        public override void OnLateUpdate()
        {
            if (everyFrame && updateType == UpdateType.OnLateUpdate)
                DoPrediction();
        }

        void DoPrediction()
        {
            go = Fsm.GetOwnerDefaultTarget(startPosition);
            startPos = go ? go.transform.position : startPositionVector.Value;
            rb = rigidbodyObject.Value.GetComponent<Rigidbody>();

            if (!rb) return;

            //do prediciton math
            posArray = PhysicsPredictionPoints(startPos, velocityDirection.Value * velocityForce.Value, 
                rb.drag, frameCount.Value, timeStepMultiplier.Value);
            
            //store only values needed in a list in case of collision options
            posList = new List<Vector3>();
            bool hit = false;
            for (int i = 0; i < posArray.Length; i++)
            {
                if (detectCollision && i != 0)
                {
                    var dir = posArray[i] - posArray[i - 1];
                    ray = new Ray(posArray[i - 1], dir.normalized);
                    hit = Physics.Raycast(ray, out rayHit, dir.magnitude, collisionMask);
                    if (hit)
                    {
                        collisionObject.Value = rayHit.collider.gameObject;
                        collisionPoint.Value = rayHit.point;
                        collisionNormal.Value = rayHit.normal;
                        break;
                    }
                }
                posList.Add(posArray[i]);
            }

            //store list positions in FSM array
            storePositions.Resize(posList.Count);
            for (int i = 0; i < posList.Count; i++)
                storePositions.Values[i] = posList[i];

        }

        //web source: https://tech.spaceapegames.com/2016/07/05/trajectory-prediction-with-unity-physics/
        Vector3[] PhysicsPredictionPoints(Vector3 _startPos, Vector3 _velocity, float _drag, int _steps, float _timeMultiplier)
        {
            Vector3[] results = new Vector3[_steps];
            results[0] = startPos;
            float timestep = ((_timeMultiplier * _timeMultiplier) / _timeMultiplier) * Time.fixedDeltaTime / Physics.defaultSolverVelocityIterations;
            Vector3 gravityAccel = Physics.gravity * timestep * timestep;
            float drag = 1f - timestep * _drag;
            Vector3 moveStep = _velocity * timestep;

            for (int i = 1; i < _steps; ++i)
            {
                moveStep += gravityAccel;
                moveStep *= drag;
                _startPos += moveStep;
                results[i] = _startPos;
            }

            return results;
        }
    }
}

